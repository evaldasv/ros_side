%% Example of using KST class for interfacing with KUKA iiwa robots
close all;clear;clc;
instrreset;
warning('off')
%% Create the robot object
ip='172.31.1.147'; % The IP of the controller
arg1=KST.LBR7R800; % choose the robot iiwa7R800 or iiwa14R820
arg2=KST.Medien_Flansch_elektrisch; % choose the type of flange
Tef_flange=eye(4); % transofrm matrix of EEF with respect to flange
iiwa=KST(ip,arg1,arg2,Tef_flange); % create the object

%% Start a connection with the server
flag=iiwa.net_establishConnection();
if flag==0
return;
end
pause(1);
disp('Control iiwa VR controller')
disp('Do not move the robot near joint limits')
disp('Do not move the robot near work space limits')

% Initial configuration
jPos={0.0, pi / 180 * 30, 0, -pi / 180 * 60, 0,...
                    pi / 180 * 90, 0};
relVel=0.15;
iiwa.movePTPJointSpace(jPos, relVel); % move to initial configuration

% IK solver parameters
numberOfIterations=10;
lambda=0.1;


%% Tool/Impedance parameters   
massOfTool=2.2; % the mass of the tool attached to flange in Kg
cOMx=0; % X coordinate of the center of mass of the tool in (mm)
cOMy=0; % Y coordinate of the center of mass of the tool in (mm)
cOMz=50; % Z coordinate of the center of mass of the tool in (mm)
cStiness=800; % cartizian stifness
rStifness=80; % rotational stifness
nStifness=100; % null space stifness

% Start the soft realtime control with impedance
iiwa.realTime_startImpedanceJoints(massOfTool,cOMx,cOMy,cOMz,...
cStiness,rStifness,nStifness);

%% Tool transform matrix
TefTool=eye(4);
TefTool(1,4)=0.0;
TefTool(2,4)=0.0;
TefTool(3,4)=0.04;

%% initial configuration vector
qin=zeros(7,1);
daINDEX=0;
for i=1:7
qin(i)=jPos{i};
end


%% Variables Initiation
[Tt,j]=iiwa.directKinematics(qin); % EEF frame transformation amtrix
rosshutdown
rosinit('172.31.1.174', 11311);
rostopic list
jog = rossubscriber('/jog_cmds')
%TtSend = rospublisher('/Tt', 'std_msgs/String');
pause(0.5);
%Ttmsg = rosmessage(TtSend);

disp('Control is activated');
disp('Use space mouse to control the robot');
%% Control loop
while true
% Calculate target transform using command, and current transform
[Tt,notImportant]=iiwa.directKinematics(qin);
%Ttmsg.Data = Tt;
%send(Ttsend, Ttmsg);
controller = receive(jog, 0.4);
Tt =   [-1 0 0 0.6+controller.Axes(4); 
        0 1 0 controller.Axes(5); 
        0 0 -1 0.4+controller.Axes(6); 
        0 0 0 1];

qt= iiwa.gen_InverseKinematics(qin, Tt,numberOfIterations,lambda );

for i=1:7
    jPos{i}=qt(i);
end
%% Send reference joints to robot
iiwa.sendJointsPositions(jPos);
%% update initial positions, target transform
qin=qt;
end

%% Turn off the direct servo
iiwa.realTime_stopDirectServoJoints();

%% turn Off blue light
iiwa.net_turnOffServer();