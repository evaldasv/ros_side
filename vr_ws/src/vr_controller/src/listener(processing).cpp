#include <ros/ros.h>
#include <sensor_msgs/Joy.h>
#include "geometry_msgs/TwistStamped.h"

float oriInputX, oriInputY, oriInputZ;
float posInputX, posInputY, posInputZ, posInputX0, posInputY0, posInputZ0, posIncrementX, posIncrementY, posIncrementZ;
float home[3];
bool once = true;
bool once2 = false;

void oriTest (float oriInput, int i)
{
  if (i==0)
  {
    oriInputX = oriInput;
  }
  if (i==1)
  {
    oriInputY = oriInput;
  }
  if (i==2)
  {
    oriInputZ = oriInput;
  }
}

void posTest (float posInput, int i, bool once)
{
  if (i==0)
  {
    posInputX = posInput;
    if(once)
    {
      posInputX0 = posInput;
      once2 = true;
    }
  }
  else if (i==2)
  {
    posInputY = posInput;
    if(once2)
    {
      posInputY0 = posInput;
      once2 = false;
    }
  }
  else if (i==1)
  {
    posInputZ = posInput;
    if(once2)
    {
      posInputZ0 = posInput;
    }
  }
  //std::cout << "posX: " << posInputX << "\r\n" << "posY: " << posInputY << "\r\n" << "posZ: " << posInputZ << "\r\n";

  posIncrementX = posInputX0 - posInputX;
  posIncrementY = posInputY0 - posInputY;
  posIncrementZ = posInputZ0 - posInputZ;

  posInputX0 = posInputX;
  posInputY0 = posInputY;
  posInputZ0 = posInputZ;

  home[0] +=  posIncrementX;
  home[1] +=  posIncrementY;
  home[2] +=  posIncrementZ;

  std::cout << "posX: " << home[0] << "\r\n" << "posY: " << home[1] << "\r\n" << "posZ: " << home[2] << "\r\n";
}

void myCallback (const sensor_msgs::Joy::ConstPtr& msg)
{
  for (unsigned i = 0; i < msg->axes.size(); ++i)
  {
    //ROS_INFO("Axis %d is now at position %f", i, msg->axes[i]);
    //oriTest(msg->axes[i],i);
  }


  for (unsigned i = 0; i < msg->buttons.size(); ++i)
  {
  //  std::cout << "buttons: " << msg->buttons[i] << "\n";
    posTest(msg->buttons[i],i,once);
    once = false;
  }

}



int main(int argc, char **argv)
{
  ros::init(argc, argv, "listener");
  ros::NodeHandle n;
  ros::Subscriber sub = n.subscribe("joy", 100, myCallback);
  ros::Publisher twist_pub_;
  twist_pub_ = n.advertise<geometry_msgs::TwistStamped>("jog_cmds", 1);
  geometry_msgs::TwistStamped twist;

  ros::Rate loop_rate(10);

  int count = 0;



  ros::spin();
  return 0;
}
