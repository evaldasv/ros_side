#include "geometry_msgs/TwistStamped.h"
#include "control_msgs/JointJog.h"
#include "ros/ros.h"
#include "sensor_msgs/Joy.h"
#include "chrono"
#include "thread"

float posInputX, posInputY, posInputZ, posInputX0, posInputY0, posInputZ0, posIncrementX, posIncrementY, posIncrementZ;
int buttonInput0, buttonInput1;
int pMode = 0;
float beta = 0.95;
float lowThreshold = 0.00001;
float posDiffX, posDiffY, posDiffZ;
float precisionLevel = 1;
int count;


namespace moveit_jog_arm
{
static const int NUM_SPINNERS = 1;
static const int QUEUE_LENGTH = 1;

class SpaceNavToTwist
{
public:
  SpaceNavToTwist() : spinner_(NUM_SPINNERS)
  {
    joy_sub_ = n_.subscribe("joy", QUEUE_LENGTH, &SpaceNavToTwist::joyCallback, this);
    twist_pub_ = n_.advertise<sensor_msgs::Joy>("jog_cmds", QUEUE_LENGTH);

    spinner_.start();
    ros::waitForShutdown();
  };

private:
  // Convert incoming joy commands to TwistStamped commands for jogging.
  // The TwistStamped component goes to jogging, while buttons 0 & 1 control
  // joints directly.
  void joyCallback(const sensor_msgs::Joy::ConstPtr& msg)
  {
    // Cartesian jogging with the axes
    sensor_msgs::Joy joy_msgs;
    joy_msgs.axes.resize(6);
    joy_msgs.buttons.resize(2);

    joy_msgs.axes[0] = msg->axes[0];
    joy_msgs.axes[1] = msg->axes[1];
    joy_msgs.axes[2] = msg->axes[2];

    buttonInput0 = msg->buttons[0];
    buttonInput1 = msg->buttons[1];

    // if (buttonInput1 == 1)
    // {
    //   if (pMode == 0)
    //   {
    //     precisionLevel = 0.5;
    //     pMode++;
    //   }
    //   else if (pMode == 1)
    //   {
    //     precisionLevel = 0.3;
    //     pMode++;
    //   }
    //   else if (pMode == 2)
    //   {
    //     precisionLevel = 1;
    //     pMode = 0;
    //   }
    //   std::this_thread::sleep_for (std::chrono::milliseconds(1000));
    // }


    // if (buttonInput1 == 1)
    //   {
    //     if (pMode == 0)
    //     {
    //       pMode = 1;
    //       precisionLevel = 0.5;
    //     }
    // else if(pMode == 1)
    //     {
    //       pMode = 0;
    //       precisionLevel = 1;
    //     }
    //   }

    posInputX = precisionLevel*(-msg->axes[3]);
    posInputY = precisionLevel*(-msg->axes[5]);
    posInputZ = precisionLevel*(-msg->axes[4]);

    while (buttonInput0)
    {
      posInputX0 = posInputX;
      posInputY0 = posInputY;
      posInputZ0 = posInputZ;
      std::cout << "reference";
      buttonInput0 =0;
    }
    posDiffX = beta*(posInputX0 - posInputX)+(1-beta)*posDiffX;
    posDiffY = beta*(posInputY0 - posInputY)+(1-beta)*posDiffY;
    posDiffZ = beta*(posInputZ0 - posInputZ)+(1-beta)*posDiffZ;

    posIncrementX += posDiffX;
    posIncrementY += posDiffY;
    posIncrementZ += posDiffZ;

    // if(posIncrementX < lowThreshold)
    // {
    //   posIncrementX = 0;
    // }
    // if(posIncrementY < lowThreshold)
    // {
    //   posIncrementY = 0;
    // }
    // if(posIncrementZ < lowThreshold)
    // {
    //   posIncrementZ = 0;
    // }

    posInputX0 = posInputX;
    posInputY0 = posInputY;
    posInputZ0 = posInputZ;

    joy_msgs.axes[3] = posIncrementX;
    joy_msgs.axes[4] = posIncrementY;
    joy_msgs.axes[5] = posIncrementZ;

    std::cout << "posX: " << joy_msgs.axes[3] << "\r\n" << "posY: " << joy_msgs.axes[4] << "\r\n" << "posZ: " << joy_msgs.axes[5] << "\r\n" << precisionLevel << "\r\n";

    twist_pub_.publish(joy_msgs);
  }

  ros::NodeHandle n_;
  ros::Subscriber joy_sub_;
  ros::Publisher twist_pub_;
  ros::AsyncSpinner spinner_;
};
}  // namespace moveit_jog_arm

int main(int argc, char** argv)
{
  ros::init(argc, argv, "spacenav_to_twist");

  moveit_jog_arm::SpaceNavToTwist to_twist;

  return 0;
}
